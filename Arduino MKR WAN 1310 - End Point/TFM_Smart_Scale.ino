/*

   MÓDULO RFID. Interconexión de pines usada:
   -----------------------------------------------
               MFRC522                Arduino
               Lector/PCD             MKR WAN 1310
   Señal       Pin                    Pin
   -----------------------------------------------
   RST/Reset   RST                    5
   SPI SS      SDA(SS)                4
   SPI MOSI    MOSI                   8 (MOSI)
   SPI MISO    MISO                   10 (MISO)
   SPI SCK     SCK                    9 (SCK) 

  PCD = Proximity Coupling Device = módulo lector RID
  PICC = Proximity Integrated Circuit Chip = tarjeta, llavero o crotal RFID

  ************************************************

  MÓDULO DE CARGA. Interconexión de pines usada:
   -----------------------------------------------
               HX711                  Arduino
                                      MKR WAN 1310
   Señal       Pin                    Pin
   -----------------------------------------------
   Datos       DT                     6
   Reloj       SCK                    7

  ************************************************

  LCD+PCF8574. Interconexión de pines usada:
   -----------------------------------------------
               PCF8574                Arduino
                                      MKR WAN 1310
   Señal       Pin                    Pin
   -----------------------------------------------
   Datos       SDA                    11 (SDA)
   Reloj       SCL                    12 (SCL)
*/

#include <SPI.h>
#include <MFRC522.h>
#include <LiquidCrystal_PCF8574.h>
#include <Wire.h>
#include <time.h>
#include <HX711.h>
#include <MKRWAN.h>
#include "arduino_secrets.h" 

// Serial Number de la smart-báscula.
#define SN "SN-001"

// Región LoRaWAN (AS923, AU915, EU868, KR920, IN865, US915, US915_HYBRID)
_lora_band region = EU868;

// Pines de RFID
#define RST_PIN                 5     
#define SS_PIN                  4

#define DEBUG_HX711

// Parámetro de calibración correspondiente a mi báscula
#define CALIBRACION 23500.0

// Pin de datos y Pin de reloj de la báscula
#define pinData_Celda_de_carga  6
#define pinClk_Celda_de_carga   7

// Número máximo de lecturas de peso por cada animal
#define MAX_LECTURAS_PESO 20

// Número de Bytes de los crotales RFID
#define N_BYTES_CROTAL 4

// Crea instancia del objeto LoRaModem
LoRaModem modem(Serial1);

// Crea instancia del objeto MFRC522
MFRC522 mfrc522(SS_PIN, RST_PIN);

// Crea instancia del objeto HX711
HX711 bascula;

// Pone la dirección del LCD a la 0x27, para un display 16x2.
LiquidCrystal_PCF8574 lcd(0x27);

int FACTOR_COMPENSACION_PV = 135;
int show = -1;
int errorLoRa = 1;

void setup() {

  int error;
  
  while (!Serial);              // No hace NADA hasta que el puerto serie esté abierto (añadido para Arduinos basados en ATMEGA32U4)
  Serial.flush();
  
  #ifdef DEBUG_HX711
    // Inicia comunicación serie
    Serial.begin(9600);
    Serial.println("#### Iniciando la báscula inteligente. Espere... ####");
  #endif

  // Inicia comunicación LoRaWAN
  if (!modem.begin(region)) {
    Serial.println("Error al iniciar el módulo LoRa. Contacte con el administrador.");
    while (1) {}
  };
  //Serial.print("El EUI del dispositivo es: ");
  //Serial.println(modem.deviceEUI());

  int connected = modem.joinOTAA(appEui, appKey);
  if (!connected) {
    Serial.println("Algo ha ido mal; quizás mala cobertura LoRaWAN. Contacte con el administrador.");
    while (1) {}
  }
  Serial.println("Conexión LoRaWAN establecida!");

  //Habilita ADR y configura factor de propagación para la comunicación LoRa
  modem.setADR(true);
  modem.dataRate(5);

  // Inicia Lector RFID
  SPI.begin();                  // Inicia SPI bus
  mfrc522.PCD_Init();           // Inicia MFRC522

  // Inicia Báscula
  bascula.begin(pinData_Celda_de_carga, pinClk_Celda_de_carga);
  // Aplica la calibración
  bascula.set_scale(CALIBRACION);
  // Inicia la tara. No debe haber nada sobre la báscula!
  bascula.tare();

  // Inicia Display
  Wire.begin();
  Wire.beginTransmission(0x27);
  error = Wire.endTransmission();

  if (error == 0) {
    lcd.begin(16, 2); // initialize the lcd
    siguienteAnimal();

  } else {
      Serial.print("Error: ");
      Serial.print(error);
      Serial.println(": LCD desconectada.");
  } // if

  //srand(time(0)); // Inicializa el generador de números aleatorios (se usa para simular una lectura de EGD)
  srand(analogRead(A0));
} // setup()

void loop() {

  byte uid[N_BYTES_CROTAL];     // Array de 4 Bytes que guardará el UID del crotal RFID
  float pv;                     // Variable que guardará el peso vivo del animal
  float egd;                    // Variable que guardará el espesor de grasa dorsal del animal

  // Hace un return vacío en caso de que el lector RFID no encuentre ningún crotal RFID; así se inicia otra vez el loop y la báscula no toma valores aún.
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Hace un return sin parámetros en caso de que no se pueda obtener información del crotal RFID detectado.
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  // Obtenemos el UID del crotal RFID
  leerUID(uid, N_BYTES_CROTAL);
  
  // Indica al crotal RFID que ya no necesitamos más información y finaliza la comunicación. El lector RFID ahorra energía.
  mfrc522.PICC_HaltA();
  
  // Hace varias lecturas del PV y aplica una media
  pv = leerPesoVivo();
  
  // Hace una lectura del EGD (aviso: generado por software en este prototipo)
  egd = leerEspesorGrasaDorsal();

  // Enviar el paquete al Gateway via LoRaWAN
  enviarPaqueteLoRaWAN(uid, N_BYTES_CROTAL, pv, egd);
  
  // Muestra PV y EGD en el display LCD
  mostrarInfoDisplay(pv, egd);
  
  // Informa al usuario que ya puede continuar con el siguiente animal
  siguienteAnimal();

} // loop()

void leerUID(byte uid[], int size) {
  
  Serial.print("UID del crotal RFID:");
  for (byte i = 0; i < size; i++) { // lee Byte a Byte
    if (mfrc522.uid.uidByte[i] < 0x10){         // da formato incluyendo un 0 en caso de que el valor leído esté entre 0 y 9.
      Serial.print(" 0");
      }
      else{
        Serial.print(" ");
      }
    Serial.print(mfrc522.uid.uidByte[i], HEX);
    uid[i] = mfrc522.uid.uidByte[i];
  }
  Serial.println("");
  
} // leerUID()

float leerPesoVivo() {
  
  float lectura, peso, sumPeso = 0;
  mostrarCalculandoDisplay();
  #ifdef DEBUG_HX711
    // Obtiene el peso MAX_LECTURAS_PESO veces
    for (int i = 0; i < MAX_LECTURAS_PESO; i++) {
      Serial.print("...Tomando peso: ");
      lectura = bascula.get_units() + FACTOR_COMPENSACION_PV;
      sumPeso = sumPeso + lectura;
      Serial.print(lectura, 2);
      Serial.print(" Kg");
      Serial.println();
    }
    peso = sumPeso / MAX_LECTURAS_PESO;
    Serial.print("PV: ");
    Serial.print(peso, 1);
    Serial.println(" kg");
    return peso;
  #endif
  return 0.0;
  
} // leerPesoVivo()

float leerEspesorGrasaDorsal() {

  float egd_generado = (float) (( rand( ) % 1501 + 1000 ) / 100.0f); // genera un número entre 10,00 y 25,00 (los rangos de espesor suelen ir de los 10 mm a los 25 mm)
  Serial.print("EGD: ");
  Serial.print(egd_generado,1);
  Serial.println(" mm");
  return egd_generado;
  
}

void mostrarInfoDisplay(float pv, float egd) {
  
  lcd.setBacklight(255);
  lcd.home();
  lcd.clear();
  lcd.setCursor(0, 0); // Línea: primera, posición: izquierda
  lcd.print("PV: ");
  lcd.print(pv,1);
  lcd.print(" kg");
  lcd.setCursor(0, 1); // Línea: segunda, posición: izquierda
  lcd.print("EGD: ");
  lcd.print(egd,1);
  lcd.print(" mm");
  delay(5000);
  
} // mostrarInfoDisplay()

void mostrarCalculandoDisplay() {

  lcd.setBacklight(0);
  lcd.clear();
  delay(500);
  lcd.setBacklight(255);
  lcd.home();
  lcd.setCursor(0, 0);
  lcd.print("Calculando...");
  
} // mostrarCalculandoDisplay()

void enviarPaqueteLoRaWAN(byte uid[], int size, float pv, float egd) {

  Serial.println("Enviando...");
  
  modem.beginPacket(); // inicializa la comunicación
  // TTN recibirá hexadecimal; lo hace directamente la librería MKRWAN.h
  for (int i = 0; i < size; i++) {
    if (uid[i] < 0x10) { // en caso de que el valor leido esté entre 0 y 9
      modem.print(0); // envía primero un 0 para mantener siempre el mismo número de Bytes enviados
      modem.print(uid[i], HEX);
    }
    else{
      modem.print(uid[i], HEX);
    }
  }
  if (pv<10.00) {
    modem.print(0); // enviar un 0 delante para que el formato en TTN sea siempre 00.00
    modem.print(pv,1);
  } else {
    modem.print(pv,1);
  }
  modem.print(egd,1);
  modem.print(SN); // envía Serial Number de la smart-báscula
  
  errorLoRa = modem.endPacket(false);

  if (errorLoRa > 0) {
    Serial.println("Listo!");
  } else {
    Serial.println("Error al enviar paquete LoRaWAN");
  }
  
} // enviarPaqueteLoRaWAN()

void siguienteAnimal() {
  
  Serial.println(F("Haga pasar el siguiente animal..."));
  Serial.println();
  lcd.setBacklight(0);
  lcd.clear();
  delay(500);
  lcd.setBacklight(255);
  lcd.home();
  if (errorLoRa > 0) {
    lcd.setCursor(0, 0);
    lcd.print("LISTO. Que pase");
    lcd.setCursor(0, 1);
    lcd.print("siguiente animal");
  } else {
    lcd.setCursor(0, 0);
    lcd.print("Error LoRa");
  }
  
} // siguienteAnimal()
