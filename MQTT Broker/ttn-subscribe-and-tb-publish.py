"""
 Este script Python se ocupa de:
    1º) Recibir datos desde el bróker de The Things Network (TTN) V3
    2º) Calcular la condición corporal a partir de los datos recibidos
    3º) Enviar los datos a la plataforma IoT ThingsBoard (TB) V3.2.2

 Autor: Adrián Martín Moliner.
 TFM "Control inteligente de la condición corporal en la cría porcina",
 Máster en "Internet of Things"
 Universidad Internacional de Valencia, 2021.
"""

# Importamos librerías paho, json y time
import paho.mqtt.client as mqtt
import ast, json, time

FIRMWARE_VERSION = "1.0.1"

# Definimos las credenciales para el cliente TTN (suscriptor)
TTN_HOST =  "eu1.cloud.thethings.network"
TTN_PORT =  1883 #MQTT port over TLS 1883; MQTT port over SSL 8883
TTN_TOPIC = "v3/smart-scale@ttn/devices/a8610a3434197d19/up"
TTN_USR =   "smart-scale" # App name
TTN_PWD =   "NNSXS.LCAESBIKS7D3H67DVQOMN66CCURCCAWAFDGWV5I.Y5YH633AJD3YTR47AKG3L7THVZIEU5ACGKVGZ2ABSL3AZROZDY3Q"

# Definimos las credenciales para el cliente TB (publicador)
TB_HOST =   "127.0.0.1" # localhost
TB_PORT =   1882 # Puerto cambiado; por defecto 1883
TB_ATTR =   "v1/devices/me/attributes" # Topic donde recibe atributos
TB_TOPIC =  "v1/devices/me/telemetry" # Topic donde recibe la telemetría (los datos)
TB_USR =    "SW5netIhfFHrObxakmoS" # Access Token del dispositivo virtual llamado smart-scale
TB_PWD =    "" # Contraseña en blanco

# Definimos mensajes para el cliente TTN (suscriptor)
def ttn_on_connect(mqttc, mosq, obj,rc):

    if rc==0:
        ttn_client.connected_flag=True #set flag
        print("Conectado cliente MQTT a The Things Network OK")
        mqttc.subscribe(TTN_TOPIC)
    else:
        print("Cliente MQTT No conectado a The Things Network. Código de retorno (rc)=",rc)
        ttn_client.loop_stop()

def ttn_on_message(mqttc,obj,msg):
    topic=msg.topic
    m_decode=str(msg.payload.decode("utf-8","ignore")) # devuelve un string json
    #print("\ndata Received type",type(m_decode))
    #print("\ndata Received",m_decode)
    m_in=json.loads(m_decode) # devuelve un objeto dict (diccionario)
    #print(type(m_in))
    #print("\ndata Converted",m_in)
    decoded_payload = m_in["uplink_message"]["decoded_payload"]
    #uid = decoded_payload["uid"]
    uid = ast.literal_eval("0x"+decoded_payload["uid"]) # convierte hexadecimal a entero
    pv =  decoded_payload["pv"]
    egd = decoded_payload["egd"]
    sn =  decoded_payload["sn"]
    print("Datos recibidos:\n\tuid: ", uid, " \n\tpv: ", pv, " Kg \n\tegd: ", egd, " mm")
    grc =   calculaGRC(pv,egd) # calcula el grado de reservas corporales
    print("GRC estimado:\n       ", grc)
    publicador(uid,pv,egd,grc,sn) # publica via mqtt en ThingsBoard

def ttn_on_subscribe(mosq, obj, mid, granted_qos):
    #print("Suscrito OK: " + str(mid) + " " + str(granted_qos))
    print ("Suscrito a \"" + TTN_TOPIC + "\" OK\n")

def ttn_on_log(mqttc,obj,level,buf):
    print("message:" + str(buf))
    print("userdata:" + str(obj))

# Definimos mensajes para el cliente ThingsBoard (publicador)
def on_log(client, userdata, level, buf):
   print(buf)

def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True #set flag
        print("[ON]\tConectando cliente MQTT a plataforma IoT ThingsBoard: OK")
    else:
        print("Cliente MQTT No conectado a plataforma IoT ThingsBoard. Código de retorno (rc)=",rc)
        client.loop_stop()

def on_disconnect(client, userdata, rc):
   print("[OFF]\tDesconectando cliente MQTT ThingsBoard: OK\n")

#def on_publish(client, userdata, mid):
    #print(" Publicado. [on_pub callback mid= "  ,mid,"]")

# Definimos el publicador mqtt para ThingsBoard
def publicador(uid,pv,egd,grc,sn):
    mqtt.Client.connected_flag=False # create flag in class
    mqtt.Client.suppress_puback_flag=False
    TBclient = mqtt.Client("ThingsBoard") #crea nueva instancia cliente mqtt
    #client.on_log=on_log
    TBclient.on_connect = on_connect
    TBclient.on_disconnect = on_disconnect
    #TBclient.on_publish = on_publish
    if TB_USR !="":
       pass
    TBclient.username_pw_set(TB_USR, TB_PWD)
    TBclient.connect(TB_HOST,TB_PORT)           # establece conexión
    while not TBclient.connected_flag: # espera en bucle
       TBclient.loop()
       time.sleep(1)
    time.sleep(3)
    # envía los atributos: firmware_version y báscula que ha tomado los datos
    attrib=dict()
    attrib["firmware_version"] = FIRMWARE_VERSION
    attrib["serial_number"] = sn
    data_out = json.dumps(attrib)
    ret_attr = TBclient.publish(TB_ATTR,data_out,0)
    # envía la telemetría: uid, pv, egd y grc
    data=dict() # declara objeto diccionario
    data["uid"]= uid
    data["pv"]=  pv
    data["egd"]= egd
    data["grc"]= grc
    data_out=json.dumps(data) # crea objeto JSON a partir del diccionario creado
    print(">>>\tPublicando en el Topic",TB_TOPIC, "; Datos de salida= ",data_out)
    ret_telemetry = TBclient.publish(TB_TOPIC,data_out,0)    # publica
    TBclient.disconnect() # desconecta el cliente ThingsBoard

# Definimos el cálculo del grado de reservas corporales a partir del peso vivo
# y del espesor de grasa dorsal, siguiendo la fórmula para el cálculo de grasa de Dourmad.
def calculaGRC(pv,egd):
    pv = pv * 0.96 # restamos un 4% al peso para no contabilizar posible ingesta
    fat = -26.4 + 0.221*pv + 1.331*egd
    if fat < 29.0:      grc = 1
    elif fat < 35.0:    grc = 2
    elif fat < 43.0:    grc = 3
    elif fat < 48.0:    grc = 4
    else:               grc = 5
    return grc

# Creamos cliente mqtt para The Things Network
ttn_client = mqtt.Client("TheThingsNetwork")
ttn_client.on_connect = ttn_on_connect
ttn_client.on_message = ttn_on_message
ttn_client.on_subscribe = ttn_on_subscribe
#ttn_client.on_log = ttn_on_log
ttn_client.username_pw_set(TTN_USR, TTN_PWD)
ttn_client.connect(TTN_HOST, TTN_PORT, 60)


# escuchamos en bucle al bróker mqtt TTN
try:
   ttn_client.loop_forever()
except KeyboardInterrupt:
   print(' \nMQTT Desconectado')
   ttn_client.disconnect()
