# TFM_IoT_2021_Sistema IoT para el control de la condición corporal en granjas de cría porcina

Este repositorio se encuentra disponible de manera pública y su contenido está
categorizado mediante directorios, a saber:

• **Arduino MKR WAN 1310 – End Point**. 
Contiene el fichero TFM_Smart_Scale.ino, su fichero auxiliar arduino_secrets.h y las librerías
necesarias. En el fichero con extensión .ino está prácticamente todo el código
que dota de funcionalidad al Arduino. Cabe decir que las líneas de código están
comentadas para facilitar su comprensión, bien para futuros programadores
que deseen aprovechar partes del código o para futuras revisiones propias del
mismo. El fichero arduino_secrets.h tan solo contiene las credenciales
necesarias para conectar con The Things Network v3.

• **Dragino-LoRaWAN-Gateway**. 
Contiene un fichero json con la configuración
del gateway LoRa utilizado en este proyecto.

• **Fritzing**.
Contiene los esquemáticos de la parte sensórica del proyecto, en dos
formatos: imágenes png y ficheros editables fzz (formato propio de la aplicación
Fritzing).

• **MQTT Broker**. 
Contiene el programa encargado de ser la pasarela MQTT
entre The Things Network v3 y ThingsBoard CE. Se trata de un fichero con
extensión py (Python).

• **ThingsBoard**. 
Contiene en formato json las cuatro alarmas definidas en este
proyecto, más la raíz root_rule_chain.json. También incluye el dashboard en
formato json.

• **Arquitectura**. 
Fichero en formato PNG que desribe la arquitectura del ecosistema IoT.
